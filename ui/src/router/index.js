import { createRouter, createWebHistory } from 'vue-router'
import Home from '../views/Home.vue'

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/ckd',
    name: 'Ckd',
    component: () => import(/* webpackChunkName: "about" */ '../views/Ckd.vue')
  },
  {
    path: '/pneumonia',
    name: 'Pneumonia',
    component: () => import(/* webpackChunkName: "about" */ '../views/Pneumonia.vue')
  },
  {
    path: '/predict-pneumonia',
    name: 'PredictPneumonia',
    component: () => import(/* webpackChunkName: "about" */ '../views/PredictPneumonia.vue')
  },
  {
    path: '/*',
    redirect: { name: 'route-name' }
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
