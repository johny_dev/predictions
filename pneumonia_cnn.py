import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import matplotlib
from keras.models import Sequential, load_model
from keras.layers import Dense
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import LabelEncoder, MinMaxScaler
import keras as k
from scipy.io.arff import loadarff 
from pymongo import MongoClient
from settings import get_settings
import os.path
from os import path

settings = get_settings()

def train():
   
    
    print("TRAIN PNEUMONIA....")
    
    if not path.exists("./.model"):

        print("LOAD DATA...")
        # Load Data
        df = pd.read_csv('./data/ckd.csv')

        # Attribute selection
        attributes_to_retain = ['sg', 'al', 'sc', 'hemo', 'pcv', 'wbcc', 'rbcc', 'htn', 'class']
        df = df.drop([col for col in df.columns if col not in attributes_to_retain], axis=1)

        # Missing values remove all rown with empty values
        df = df.dropna(axis=0)

        # Transform non numeric data
        for col in df.columns:
            if df[col].dtype != np.number:
                df[col] = LabelEncoder().fit_transform(df[col])

        # Split data to X independent - features and y denpedent - target
        X = df.drop(['class'], axis=1)
        y = df['class']

        # Connect to MongoDB
        client =  MongoClient(settings.MONGODB_URL)
        db = client['ml']
        collection = db['ckd_data']
        if collection.count():
            collection.drop()

        collection = db['ckd_data']
        data_dict = df.to_dict("records")# Insert collection
        collection.insert_many(data_dict)

        # Feature Scalling [0-1]
        x_scaler = MinMaxScaler()
        X[X.columns] = x_scaler.fit_transform(X[X.columns])

        # Split 80-20 training test & shuffle
        X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, shuffle=True)

        model = Sequential()
        model.add(Dense(256, input_dim = len(X.columns), kernel_initializer=k.initializers.random_normal(seed=13), activation='relu'))
        model.add(Dense(1, activation='hard_sigmoid'))

        # Compile
        model.compile(loss=k.losses.BinaryCrossentropy(), optimizer='adam', metrics=['accuracy'])

        # Train model
        history = model.fit(X_train, y_train, epochs=2000, batch_size=X_train.shape[0])

        model.save('ckd.model')
        plt.plot(history.history['accuracy'])
        plt.plot(history.history['loss'])

        plt.title("CKD Model accuracy and loss")
        plt.ylabel("Model accuracy and loss")
        plt.xlabel("epoch")

        matplotlib.pyplot.savefig('./static/ckd_ann.png')

        print("FINISHED")

train()
