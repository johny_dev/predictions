import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import PrimeVue from 'primevue/config';

import "primevue/resources/themes/md-light-deeppurple/theme.css"      
import "primevue/resources/primevue.min.css"              
import "primeicons/primeicons.css"
import 'primeflex/primeflex.css';
import '@/assets/App.scss';

const app = createApp(App)

app.use(PrimeVue)
app.use(router)

app.mount('#app')
