from pydantic import BaseSettings, Field
from functools import lru_cache
from typing import List

class Settings(BaseSettings):
    APP_NAME: str = "ML HealthData"
    APP_STATE: str = Field("prod", env="APP_STATE")
    
    MONGODB_URL: str = Field("mongodb://root:root@localhost:27017", env="MONGODB_URL")
    DB_NAME: str = Field("ml", env="DB_NAME")
    MAX_CONNECTIONS_COUNT: int = Field(15, env="MAX_CONNECTIONS_COUNT")
    MIN_CONNECTIONS_COUNT: int = Field(10, env="MIN_CONNECTIONS_COUNT")
    MAX_CURSOR_LENGTH: int = Field(100000000, env="MIN_CONNECTIONS_COUNT")

    class Config:
        env_file = '.env'
        env_file_encoding = 'utf-8'
    
@lru_cache()
def get_settings():
    return Settings()