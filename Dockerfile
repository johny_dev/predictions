FROM python:3.8

COPY ./requirements.txt .
RUN pip install -r requirements.txt

COPY . /app
#COPY requirements.txt ./app/

WORKDIR /app

RUN  python ckd_ann.py 

CMD ["uvicorn", "api:app", "--host", "0.0.0.0", "--port", "80"]