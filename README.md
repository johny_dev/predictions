# Health ML experiment

This is an experimental project to test combination of docker with analytics tools
The project consists of 4 separate docker containers

- Mongo Container as a persistence layer downloaded from original repo
- ml-app Python ML Backend and Fastapi Web Infrasructure LAyer which expose the ML algos
- vue.js node for building frontend code
- nginx Web server which expose the application


## Instructions

1. Clone Repository
```
git clone https://gitlab.com/johny_dev/predictions.git
```

2. Visit predictions directory
```
cd predictions
```

3. Run docker-composer
```
docker-compose up
```

4. Visit [Health ML Experiment](http://localhost:91)

You could make changes in the docker-compose.yml file to change properties