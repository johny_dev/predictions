from fastapi import FastAPI, Depends, File, UploadFile, Form,  HTTPException,  Response
from motor.motor_asyncio import AsyncIOMotorClient
import logging
from settings import get_settings, Settings
from fastapi import Depends
from databases import DatabaseURL
from pydantic import BaseModel
from typing import List, Dict, Optional
from datetime import datetime, date
from fastapi.responses import JSONResponse
import keras as k
import pandas as pd
from fastapi.middleware.cors import CORSMiddleware
import numpy as np
from sklearn.preprocessing import LabelEncoder, MinMaxScaler
from dicom_keras import DicomImageDataGenerator
from utils import *
import PIL
from dataclasses import InitVar
import json
import shutil
import pydicom
import io
import matplotlib.pyplot as plt
import PIL
from starlette.responses import StreamingResponse

app = FastAPI()

origins = [
    "*"
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

### Exceptions ###
class ApplicationException(Exception):
    def __init__(self, message, status_code=400):
        self.message = message
        self.status_code = status_code

@app.exception_handler(ApplicationException)
async def request_validation_exception_handler(request, exc: ApplicationException):
    return JSONResponse(
        status_code=exc.status_code if hasattr(exc, 'status_code') else 400,
        content={"message": exc.message})

### Mongo ###
class DataBase:
    client: AsyncIOMotorClient = None


db = DataBase()

async def get_database() -> AsyncIOMotorClient:
    return db.client

async def connect_to_mongo():
    settings = get_settings() 

    db.client = AsyncIOMotorClient(str(DatabaseURL(settings.MONGODB_URL)),
                                   maxPoolSize=settings.MAX_CONNECTIONS_COUNT,
                                   minPoolSize=settings.MIN_CONNECTIONS_COUNT,
                                   uuidRepresentation="standard")

    print("LOAD DATA...")
    # Load Data
    df = pd.read_csv('./data/ckd.csv')

    # Attribute selection
    attributes_to_retain = ['sg', 'al', 'sc', 'hemo', 'pcv', 'wbcc', 'rbcc', 'htn', 'class']
    df = df.drop([col for col in df.columns if col not in attributes_to_retain], axis=1)

    # Missing values remove all rown with empty values
    df = df.dropna(axis=0)

    # Transform non numeric data
    for col in df.columns:
        if df[col].dtype != np.number:
            df[col] = LabelEncoder().fit_transform(df[col])

    # Split data to X independent - features and y denpedent - target
    X = df.drop(['class'], axis=1)
    y = df['class']

    # Connect to MongoDB

    collection = db.client['ml']['ckd_data']

    if collection:
        collection.drop()
    


    collection = db.client['ml']['ckd_data']
    data_dict = df.to_dict("records")# Insert collection
    collection.insert_many(data_dict)

async def close_mongo_connection():
    db.client.close()

app.add_event_handler("startup", connect_to_mongo)
app.add_event_handler("shutdown", close_mongo_connection)

### Requests ###

class PredictCkdRequest(BaseModel):
    specific_gravity: float
    albumin: float
    serum_creatinine: float
    hemoglobin: float
    packed_cell_volume: float
    white_cells_count: float
    red_cells_count: float
    hypertension: bool

class PredictPnRequest(BaseModel):
    patient_first_name: str
    patient_last_name: str
    dob: datetime
    dicom_img: Optional[str] = None



### Models ###
class PnPatientData(PredictPnRequest):
    dob: date

    def transform_image(self):
        img = load_img(self.dicom_img).resize((224, 224)).convert('L')
       
        img = img_to_array(img)
        img = img * 1./255
        
        return np.array([img])

class CkdPatientData(PredictCkdRequest):
    pass
    
    def hypertension_number(self):
        return (self.hypertension and 1) or 0

    def normalize(self, min_max_dict: Dict):
        def n(x:float, min: float, max: float):
            return round((x - min) / (max - min), 6)
        
        attrs = self.dict()
        attrs["hypertension"] = self.hypertension_number()
        normalized = {}
        for attr, value in attrs.items():
            normalized[attr] = n(value, min_max_dict[attr]['min'],  min_max_dict[attr]['max'])


        return normalized

class PnPrediction(BaseModel):
    patient_data: PnPatientData
    prediction_datetime: datetime
    prediction: Optional[bool] = None
    dt_str: Optional[str] = None

    def predict(self) -> bool:

        try:
            model = k.models.load_model('pn.model')
        except:
            raise ApplicationException("Model Pneumonia was not found")

        sample = self.patient_data.transform_image()
        
        prediction = model.predict_classes(sample)
        print(prediction)
        #prediction  = np.argmax(prediction , axis = 1)
        self.prediction =  bool(int(prediction[0]) == 1)

class CkdPrediction(BaseModel):
    patient_data: CkdPatientData
    prediction_datetime: datetime
    prediction: Optional[bool] = None
    dt_str: Optional[str] = None

    def predict(self, min_max_dict: Dict) -> bool:

        try:
            model = k.models.load_model('ckd.model')
        except:
            raise ApplicationException("Model Ckd was not found")

        normalized_data = self.patient_data.normalize(min_max_dict)

        tr = [
            normalized_data['specific_gravity'],
            normalized_data['albumin'],
            normalized_data['serum_creatinine'],
            normalized_data['hemoglobin'],
            normalized_data['packed_cell_volume'],
            normalized_data['white_cells_count'],
            normalized_data['red_cells_count'],
            normalized_data['hypertension']
        ]
        sample = pd.DataFrame([tr], columns = ['sg', 'al', 'sc', 'hemo', 'pcv', 'wbcc', 'rbcc', 'htn'])
        
        prediction = model.predict(sample)[0][0]

        self.prediction = bool(prediction < 0.5)



### Repository ###
class BaseRepository():
    def __init__(self, client: AsyncIOMotorClient = Depends(get_database)):
        self.db = client['ml']


class PnPredictionsRepository(BaseRepository):
    async def get_all(self):
        result = []
        collection = self.db["pn_predictions"]
        cursor = collection.find({}).sort('prediction_datetime', -1)

        for row in await cursor.to_list(1000000):
            #row['patient_data']["dob"] = row['patient_data']["dob"].strftime("%Y-%m-%d")
            p_d = PnPatientData(**row['patient_data'])
            dt=row['prediction_datetime'].strftime("%Y-%m-%d")
            p = PnPrediction(patient_data=p_d, prediction=bool(row['prediction']), prediction_datetime=row['prediction_datetime'],  dt_str=dt)
            result.append(p)

        return result

    async def persist(self, prediction: PnPrediction):
        collection = self.db["pn_predictions"]
        d = prediction.dict()
        d["patient_data"]["dob"] = prediction.patient_data.dob.strftime("%Y-%m-%d")
        d["prediction"] = int(d["prediction"])
        await collection.insert_one(d)

class PredictionsRepository(BaseRepository):
    async def get_all(self):
        result = []
        collection = self.db["ckd_predictions"]
        cursor = collection.find({}).sort('prediction_datetime', -1)

        for row in await cursor.to_list(1000000):
            p_d = CkdPatientData(**row['patient_data'])
            dt=row['prediction_datetime'].strftime("%Y-%m-%d")
            p = CkdPrediction(patient_data=p_d, prediction=bool(row['prediction']), prediction_datetime=row['prediction_datetime'],  dt_str=dt)
            result.append(p)

        return result

    async def persist(self, prediction: CkdPrediction):
        collection = self.db["ckd_predictions"]
        d = prediction.dict()
        d["patient_data"]["hypertension"] = int(d["patient_data"]["hypertension"])
        d["prediction"] = int(d["prediction"])
        await collection.insert_one(d)
    

c_d = {'sg': 'specific_gravity', 'al': 'albumin', 'sc': 'serum_creatinine', 'hemo': 'hemoglobin', 'pcv': 'packed_cell_volume', 'wbcc': 'white_cells_count', 'rbcc': 'red_cells_count', 'htn': 'hypertension'}

class DataRepository(BaseRepository):
    
    async def get_min_max(self):
        columns = ['sg', 'al', 'sc', 'hemo', 'pcv', 'wbcc', 'rbcc', 'htn']
        response = {}
        collection = self.db["ckd_data"]

        for c in columns:
            d_min = {}
            d_min[c]=1

            d_max = {}
            d_max[c]=-1
            
            dl = {}
            dl[c]=1

            min = await collection.aggregate([
                {'$sort': d_min},
                {'$limit': 1},
                {'$project': dl}
            ]).to_list(length=1)

            max = await collection.aggregate([
                {'$sort': d_max},
                {'$limit': 1},
                {'$project': dl}
            ]).to_list(length=1)

            response[c_d[c]] = {
                'min': min[0][c],
                'max': max[0][c]
            }

        
        return response


### Use Case ###
class PredictCkd():
    def __init__(self, data_repo: DataRepository = Depends(), predictions_repo: PredictionsRepository = Depends()):
        self.data_repo = data_repo
        self.predictions_repo = predictions_repo
    
    async def handle(self, request: PredictCkdRequest) -> CkdPrediction:

        min_max_dict = await self.data_repo.get_min_max()

        patient_data = CkdPatientData(**request.dict())
        dt=datetime.now().strftime("%Y-%m-%d")
        prediction = CkdPrediction(
            patient_data=patient_data,
            prediction_datetime= datetime.now(),
            dt_str=dt
        )

        prediction.predict(min_max_dict)

        await self.predictions_repo.persist(prediction)

        return prediction.dict()

class PredictPn():
    def __init__(self, predictions_repo: PnPredictionsRepository = Depends()):
        self.predictions_repo = predictions_repo
    
    async def handle(self, request: PredictPnRequest) -> PnPrediction:
        req_dict = request.dict()
        req_dict['dob'] = req_dict['dob'].date()

        patient_data = PnPatientData(**req_dict)
        dt=datetime.now().strftime("%Y-%m-%d")
        prediction = PnPrediction(
            patient_data=patient_data,
            prediction_datetime= datetime.now(),
            dt_str=dt
        )

        prediction.predict()

        await self.predictions_repo.persist(prediction)

        return prediction.dict()

class DicomToImg():
    async def handle(self, dicom: UploadFile):
        dicom = await dicom.read()
        dicom = io.BytesIO(dicom)
        dicom = pydicom.dcmread(dicom)
        img = dicom.pixel_array # get image array
        img = PIL.Image.fromarray(img)
        
        byte_io = io.BytesIO()
        img.save(byte_io, 'PNG')

        return byte_io


class GetPredictions():
    def __init__(self, predictions_repo: PredictionsRepository = Depends()):
        self.predictions_repo = predictions_repo
    
    async def handle(self) -> List[CkdPrediction]:

        return {'data': await self.predictions_repo.get_all()}

class GetPnPredictions():
    def __init__(self, predictions_repo: PnPredictionsRepository = Depends()):
        self.predictions_repo = predictions_repo
    
    async def handle(self) -> List[PnPrediction]:

        return {'data': await self.predictions_repo.get_all()}

### Routes ###

@app.post('/ckd/predict')
async def ckd_predict(req: PredictCkdRequest, service: PredictCkd = Depends()):
    return await service.handle(req)

@app.get('/ckd/predictions')
async def ckd_get_predictions(service: GetPredictions = Depends()):
    return await service.handle()


@app.post('/dicom-to-img')
async def dicom_to_img(file: UploadFile = File(...), service: DicomToImg = Depends()):
    bytes = await service.handle(file)
    bytes.seek(0)
    return StreamingResponse(bytes, media_type="image/png")
    

@app.get('/pneumonia/predictions')
async def pn_get_predictions(service: GetPnPredictions = Depends()):
    return await service.handle()

@app.post('/pneumonia/predict')
async def pn_predict(metadata = Form(...), file: UploadFile = File(...), service: PredictPn = Depends()):

    try:
        metadata = json.loads(metadata)
    except:
        raise HTTPException(status_code=400, detail="Invalid Json Format")

    req = PredictPnRequest(**metadata)
 
    path_to_save = "./data/pneumonia_images/{name}".format(name=file.filename)
    with open(path_to_save, "wb") as buffer:
        shutil.copyfileobj(file.file, buffer)

    req.dicom_img = path_to_save
    
    return await service.handle(req)
